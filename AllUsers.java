import java.util.HashSet;

import java.util.Iterator;

@SuppressWarnings("unused")
public class AllUsers {
	public static final AllUsers currentUser = null;
	/*
	 * To create Allusers class to display all the users
	 * @author Thriveni
	 */
	public String name;
	public String age;
	public String profession;
	public String hobbie;
	String[] hobbies = new String[10];
	/**
	 * To create a constructor with arguments for the User class
	 * @param name
	 * @param age
	 * @param profession
	 * @param hobbie
	 * @param hobbies
	 */
	public AllUsers(String name, String age, String profession, String hobbie, String[] hobbies) {
		this.name = name; // using this statement
		this.age = age; // using this statement
		this.profession = profession; // using this statement
		this.hobbie = hobbie; // using this statement
		this.hobbies = hobbies; // using this statement
		System.out.println(this);
	}
	/**
	 * override the default toString method of Message so that message can be
	 * printed in human readable format
	 */
	@Override
	public String toString() {
		String StringToReturn = "";
		StringToReturn +="name               :      " + this.name +          "\n";
		StringToReturn +="age                :      " + this.age +           "\n";
		StringToReturn +="profession         :      " + this.profession +    "\n";
		StringToReturn +="primary hobbie     :      " + this.hobbie +        "\n";

		String hobbyString = "[";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbyString += "\"" + this.hobbies[i] + "\" ";
		}
		hobbyString += "]";

		StringToReturn += " other hobbies   :     " + hobbyString + "\n";
		return StringToReturn;
	}
	
	public String getname() {
		return name;
	}
	
	}

	

