import java.util.ArrayList;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AllUserCollection<Allusers> {
	/**
	 * To create a AllUserCollection class to implement the UserCollection by using
	 * ArrayList
	 * @author Thriveni
	 *
	 */
	ArrayList<AllUsers> userList = new ArrayList<AllUsers>();
	ArrayList<AllUsers> selectedList = new ArrayList<AllUsers>();
	public Object createNewUser;
	/**
	 * To create createNewUser method using parameters by using ArrayList and
	 * addUser
	 * @param name
	 * @param age
	 * @param profession
	 * @param hobbie
	 * @param hobbies
	 */
	@SuppressWarnings("unchecked")
	public boolean createNewUser(String name, String age, String profession, String hobbie, String[] hobbies) {
		
		if (!validateName(name)) {
			System.out.println("name validation fails");
			return false;
		}
			else {
				AllUsers newBlunderUser=new AllUsers(name,age,profession,hobbie,hobbies);
				this.userList.add(newBlunderUser);
				return true;
			}
			
	}
	
	/**
	 * This is a helper method to validate name parameter while creating new user object.
	 * This name has to be unique.
	 * @param name
	 * @return true if validation passes, false if validation fails
	 */
		private boolean validateName(String name) {
			
			Pattern pattern = Pattern.compile("Thriveni");
		    Matcher matcher = pattern.matcher(name);
		    if(matcher.find()) {
		    	// now checking for duplicates
		    	boolean isDuplicateFound = false;
		    	for (int i = 0; i < this.userList.size(); i++) {
					AllUsers currentUser = this.userList.get(i);
					if (currentUser.getname().toLowerCase().trim().equals(name.toLowerCase().trim())) {
						isDuplicateFound = true;
					}
				}
		    	if (isDuplicateFound) {
		    		return false; // duplicate username not allowed
		    	} else {
		    		return true;
		    	}
		    } else {
		    	return false;
		    }
		}
	public void printAllUsers() {
		for (AllUsers user : this.userList) {
			System.out.println(user);
		}
	}
	/**
	 * To create findMatchedUser method using parameter by using ArrayList and
	 * addUser
	 * @param username
	 * 
	 * */

	@SuppressWarnings("unchecked")
	public Iterable<AllUsers> findMatchedUser(String username) {

		@SuppressWarnings("unused")
		AllUsers foundUser;
		for (AllUsers currentUser : userList) {
			if (currentUser.name.contains(username)) {
				foundUser = currentUser;
				selectedList.add(foundUser);
				for (Iterator<AllUsers> iter = userList.iterator(); iter.hasNext();) {
					AllUsers currentuser = (AllUsers) iter.next();

					if (currentuser.name.contains(username)) {
						selectedList.add(currentuser);
					}
					// selectedList.addAll((Collection<? extends String>) findUser);
				}
			}

		}
		return selectedList;

	}
}